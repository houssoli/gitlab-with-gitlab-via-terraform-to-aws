variable "region" {
  default     = "us-east-2"
  description = "The name of the AWS region you'd like to deploy in."
}

variable "gitlab_version" {
  default = "12.8.5"
  description = "Community build of EE are numbered vs. latest"
}
variable "availability_zone" {
  default = "us-east-2b"
}

# Using the default VPC in the selected region for this demo.
data "aws_vpc" "default" {
  default = true
}

variable "ssh_key" {
  # default = "kelly-mbp"
  default     = "cs-demo"
  description = "This is the name of your provisioning machine's public SSH key"
}

variable "GitLabEE_EC2_instance_type" {
  # default = "t3.large"
  default = "t3a.large"
}

variable "GitLabRunner_instance_type" {
  default = "t3.medium"
}

variable "SG-Prefix" {
  default     = "KH-GitLabEE_Demo-"
  description = "Prefix to add to security groups"
}

variable "ttl" {
  default     = 30
  description = "Default DNS time to live for a record. Variable set to 30 seconds"
}

variable "private_ssh_key_dir" {
  default     = "/Users/kelly/.ssh/cs-demo.pem"
  description = "Location of the PRIVATE key provisioning infrastructure."
}

variable "GL_Domain" {
  default = "gl-demo.io"
}

variable "GL_Host_Name" {
  default = "gitlab."
}
